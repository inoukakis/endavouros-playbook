
    . venv/bin/activate
    pip install -r requirements.txt
    ansible-galaxy install gantsign.oh-my-zsh
    ansible-galaxy collection install kewlfft.aur
    ansible-playbook -i localhost playbook-endavourOs.yml --ask-become-pass